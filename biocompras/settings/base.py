import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = '-gpi2l5_9wti#im-!i(83^a++sa8yhctj@9l0&hrsjg66q-ill'

LANGUAGE_CODE = 'es-co'

TIME_ZONE = 'America/Bogota'

USE_I18N = True

USE_L10N = True

USE_TZ = True

DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    )

THRID_PARTY_APPS = (
    'widget_tweaks',
    'storages',
    'social_django',
    )

LOCAL_APPS = (
	'apps.tiendas',
	'apps.usuarios',
    'apps.home',
    'apps.carrito',
    'apps.blog',
    )



INSTALLED_APPS = DJANGO_APPS + THRID_PARTY_APPS + LOCAL_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'social_django.middleware.SocialAuthExceptionMiddleware',
]

ROOT_URLCONF = 'biocompras.urls'


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, '../templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ],
        },
    },
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = (
    'biocompras.estandar.backends.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',
    'social_core.backends.facebook.FacebookAppOAuth2',
    'social_core.backends.facebook.FacebookOAuth2',
    )

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.mail.mail_validation',
    'social_core.pipeline.social_auth.associate_by_email',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
    'apps.usuarios.pipelines.get_avatar',
    'apps.usuarios.pipelines.create_store',
)

WSGI_APPLICATION = 'biocompras.wsgi.application'

SESSION_SAVE_EVERY_REQUEST=True

AUTH_USER_MODEL = 'usuarios.CustomUser'

LOGIN_URL = '/users/login/'
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
SOCIAL_AUTH_URL_NAMESPACE = 'social'
#SOCIAL_AUTH_USER_MODEL = 'usuarios.User'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
  'locale': 'es_CO',
  'fields': 'id, name, email, age_range'
}


SOCIAL_AUTH_FACEBOOK_KEY = '177391072858513'
SOCIAL_AUTH_FACEBOOK_SECRET = 'fe77c746b4e2a74d3ff046fbf010625b'


# configuraciones de DROPBOX
DROPBOX_ROOT_PATH = '/biocompras/'
DEFAULT_FILE_STORAGE = 'storages.backends.dropbox.DropBoxStorage'
DROPBOX_OAUTH2_TOKEN = 'JXLOeHROIAAAAAAAAAABtYy4XDEkfdHE4TGlXbyULaWNPgypf0RC7C8id-k4bssZ'

MEDIA_ROOT = '/biocompras/'
MEDIA_URL = '/biocompras/'
