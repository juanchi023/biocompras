from django.contrib.auth.forms import UserCreationForm


class UserCreationMixin(object):
    def save(self, commit=True):
        user = super(UserCreationMixin, self).save(commit)
        user.is_active = False
        if commit:
            user.save()
        return user
