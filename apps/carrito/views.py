from django.shortcuts import render
from apps.tiendas.models import *
from apps.usuarios.models import *
from .models import *
from django.contrib.auth import get_user_model
import datetime
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseServerError
from django.urls import reverse
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
try:
    from django.utils import simplejson as json
except ImportError:
    import json

User = get_user_model() 


@require_POST
def add_to_shopcart(request, *args, **kwargs):

	p_id = int(request.POST.get('product_detail_id', None))
	cant = int(request.POST.get('cantidad', None))

	product_detail = ProductDetail.objects.get(id=p_id)

	shop_cart = ShopCart.objects.get(owner=request.user)

	items = shop_cart.items.all()

	present = False

	for cart_item in items:
		
		if cart_item.product_detail == product_detail:
			cart_item.quant += cant
			present = True
			cart_item.save()
			if cant == 1:
				message = 'Se ha añadido 1 {1} a su carrito de compra'.format(cant,cart_item.product_detail.product.name)

			if cant > 1: 
				message = 'Se han añadido ({0}) {1} a su carrito de compra'.format(cant,cart_item.product_detail.product.name)

			shop_cart.total += cart_item.product_detail.price * cant
			shop_cart.prod_quant += cant
			shop_cart.save()

	if present == False:
		prod = CartItems()
		prod.shopcart = shop_cart
		prod.product_detail = product_detail
		prod.quant = cant
		prod.save()
		if cant == 1:
				message = 'Se ha añadido 1 {1} a su carrito de compra'.format(cant,prod.product_detail.product.name)

		if cant > 1: 
			message = 'Se han añadido ({0}) {1} a su carrito de compra'.format(cant,prod.product_detail.product.name)
		
		shop_cart.total += prod.product_detail.price * cant
		shop_cart.prod_quant += cant
		shop_cart.save()

	ctx = {'message': message}
    # use mimetype instead of content_type if django < 5
	return HttpResponse(json.dumps(ctx), content_type='application/json')

@login_required
def my_cart(request):
    
    shop_cart = ShopCart.objects.get(owner=request.user)
    cart_items = CartItems.objects.filter(shopcart=shop_cart)[:3]

    cart_items2 = CartItems.objects.filter(shopcart=shop_cart)

 
    return render(request,  'users/cart.html',{'shop_cart':shop_cart,
        'cart_items':cart_items, 'cart_items2':cart_items2})

@require_POST 
def delete_car_item(request):

	if request.method == 'POST':

		total = 0
		cant = 0

		shop_cart = ShopCart.objects.get(owner=request.user)
		
		id_product = request.POST.get('id_product', None)

		product = CartItems.objects.get(id=id_product)
		product.delete()

		cart_items = CartItems.objects.filter(shopcart=shop_cart)

		for product in cart_items:
			total += product.quant * product.product_detail.price
			cant+=1

		shop_cart.total = total
		shop_cart.prod_quant = cant
		shop_cart.save()


		message = 'Producto eliminado de su carrito de compra'
		ctx = {'message': message}
    # use mimetype instead of content_type if django < 5
	return HttpResponse(json.dumps(ctx), content_type='application/json')


@require_POST 
def clean_cart(request):

	if request.method == 'POST':
		
		shop_cart = ShopCart.objects.get(owner=request.user)
		cart_items = shop_cart.items.all()

		for product in cart_items:
			product.delete()

		shop_cart.total = 0
		shop_cart.prod_quant = 0
		shop_cart.save()

		message = 'Su Carrito de compra de ha vaciado'
		ctx = {'message': message}
	return HttpResponse(json.dumps(ctx), content_type='application/json')