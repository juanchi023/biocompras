from django.db import models
from django.contrib.auth import get_user_model
from apps.tiendas.models import *

User = get_user_model() 

class ShopCart(models.Model):
	owner = models.ForeignKey(User, on_delete=models.CASCADE)
	total = models.DecimalField(max_digits=13, decimal_places=2, default=0)
	prod_quant = models.IntegerField(default=0)


	def __str__(self):
		return "Carrito de {0} - Cantidad de productos ({1}) - Total neto: $ {2}".format(self.owner, self.prod_quant, self.total)


class CartItems(models.Model):

	shopcart = models.ForeignKey(ShopCart, on_delete=models.CASCADE, related_name='items')
	product_detail = models.ForeignKey(ProductDetail, on_delete=models.CASCADE)
	quant = models.IntegerField(default=1)


	def __str__(self):
		return "{0} - Cantidad: {1}, en el carrito de {2}".format(self.product_detail,self.quant,self.shopcart.owner)
