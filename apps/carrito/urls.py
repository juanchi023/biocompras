from django.urls import path
from . import views


urlpatterns = [
    path('add_to_shopcart/', views.add_to_shopcart, name='add_to_shopcart'),    
	path('my-cart/', views.my_cart, name="my_cart"),
	path('delete-car-item/', views.delete_car_item, name="delete_car_item"),
	path('clean-cart/', views.clean_cart, name="clean_cart"),
    ]
