from django.urls import include, path

from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    # ex: /polls/
    path('login/', views.login_view, name='custom_login'),
    path('logout/', views.logout_view, name='logout_view'),
	path('', include('social_django.urls', namespace='social')),
	path('login_ajax/', views.login_ajax, name='login_ajax'),
    path('profile/', views.user_profile, name='user_profile'),
]
