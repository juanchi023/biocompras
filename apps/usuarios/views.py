from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import login,logout, authenticate
from .forms import EmailAuthenticationForm, UpdateProfileForm
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from apps.tiendas.models import *
from apps.carrito.models import ShopCart
try:

    from django.utils import simplejson as json
except ImportError:
    import json
from django.views.decorators.http import require_POST
from .forms import RegisterForm

def login_view(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        form = EmailAuthenticationForm()
        register_form = RegisterForm(request.POST or None)

        if register_form.is_valid():
            user = register_form.save(commit=False)
            user.username = user.email
            user.save()
            tienda = Store()
            tienda.owner = user
            tienda.save()
            carrito = ShopCart()
            carrito.owner = user
            carrito.save()
            return HttpResponseRedirect(reverse('home'))
        return render(request, 'users/login.html', {'form':form, 'register_form':register_form})

def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('custom_login'))


@require_POST
def login_ajax(request):
    if request.method == 'POST':
        login_form = EmailAuthenticationForm(request.POST or None)

        if login_form.is_valid():
            errors = login_form.errors
            user = login(request, login_form.get_user())
            data = json.dumps(errors)
            return HttpResponse(data, content_type='application/json')
        else:
            errors = login_form.errors
            return HttpResponse(json.dumps(errors))

@login_required
def user_profile(request):

    form = UpdateProfileForm(request.POST or None, request.FILES or None, instance=request.user)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('user_profile'))

    return render(request,"users/account_profile.html",{'form':form})
