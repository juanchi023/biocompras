from django.shortcuts import render
from social_core.pipeline.partial import partial
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.core.files.base import ContentFile
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from urllib.request import urlopen
from apps.tiendas.models import *
from apps.carrito.models import *
#import sendgrid
import os
try:
    # Python 3
    import urllib.request as urllib
except ImportError:
    # Python 2
    import urllib2 as urllib
#from tecnix.settings.base import SENDGRID_API_KEY



@partial
def send_email(backend,strategy, details, response, is_new=False, user=None, *args, **kwargs):
    if is_new:
        nombre_apellidos = "{0} {1}".format(user.first_name, user.last_name)
        email = user.email
        backend = backend.name
        send_email_gracias(nombre_apellidos,email,backend)
        


    

def send_email_gracias(nombre_apellidos,email,backend):
    sg = sendgrid.SendGridAPIClient(apikey=SENDGRID_API_KEY)
    if backend == 'google-oauth2':
        desde= 'Usted se registró con su cuenta de Google'
    elif backend == 'facebook':
        desde= 'Usted se registró con su cuenta de Facebook'
    data = {
      "personalizations": [
        {
          "to": [
            {
              "email": email
            }
          ],
          "substitutions": {
            "-cliente-": nombre_apellidos,
            "-email-": email,
            "-password-": desde,
          },
        },
      ],
      "from": {
        "email": "hola@tecnix.co"
      },
      "content": [
        {
          "type": "text/html",
          "value": "I'm replacing the <strong>body tag</strong>"
        }
      ],
      "template_id": "017d389b-9a57-46b8-bff9-b90b0ee43fe2"
    }
    try:
        response = sg.client.mail.send.post(request_body=data)
    except urllib.HTTPError as e:
        print (e.read())
        exit()
    print(response.status_code)

def get_avatar(backend, details, response, is_new=False, user=None, *args, **kwargs):
    url = None
    if backend.name == 'facebook':
        url = 'https://graph.facebook.com/%s/picture?type=large' % response['id']
        image_name = 'fb_avatar_%s.jpg' % response['id']
        image_stream = urlopen(url)
        user.img_profile.save(
            image_name,
            ContentFile(image_stream.read()),
            )
        user.save() 

@partial
def create_store(backend,strategy, details, response, is_new=False, user=None, *args, **kwargs):
    if is_new:
        tienda = Store()
        tienda.owner = user
        tienda.save()
        carrito = ShopCart()
        carrito.owner = user
        carrito.save()

