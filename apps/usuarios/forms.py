from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate
from .models import CustomUser


User = get_user_model()

class RegisterForm(UserCreationForm):

    def clean_email(self):
        data = self.cleaned_data['email']

        if User.objects.filter(email=data).exists():
            raise forms.ValidationError("Este email ya lo tiene otra empresa o departamento")

        return data

    class Meta():
        model = CustomUser
        fields  = ('first_name','last_name', 'email', 'phone')


class EmailAuthenticationForm(forms.Form):
    email = forms.EmailField(required=True)
    password = forms.CharField(label='Password', widget=forms.PasswordInput,required=True)

    def __init__(self, *args, **kwargs):
        self.user_cache = None
        super(EmailAuthenticationForm, self).__init__(*args,**kwargs)

    def clean(self):
        self.cleaned_data = super(EmailAuthenticationForm, self).clean()
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        self.user_cache = authenticate(email=email, password=password)

        if self.user_cache is None:
            self._errors["email"] = self.error_class(['¡Usuario o clave Incorrecta!'])
        elif not self.user_cache.is_active:
            self._errors["email"] = self.error_class(['¡Usuario Inactivo! Verifique con el administrador de su empresa.'])

        return self.cleaned_data

    def get_user(self):
        return self.user_cache

class UpdateProfileForm(forms.ModelForm):
    class Meta():
        model = CustomUser
        fields  = ('first_name','last_name', 'email', 'phone', 'img_profile',)
