from django.db import models
import os
from django.contrib.auth.models import AbstractUser, UserManager

class CustomUser(AbstractUser):

	def upload_img_user(instance, filename):
		filename_base, filename_ext = os.path.splitext(filename)
		print(instance.username)
		return 'users/{0}/imgs/{1}{2}'.format(instance.username,'profile',filename_ext.lower())
	
	location = models.CharField(max_length=100)
	addres = models.CharField(max_length=255)
	phone = models.CharField(max_length=30)
	dni = models.CharField(max_length=20, unique=True)
	is_vendor = models.BooleanField(default=False)
	img_profile = models.ImageField(upload_to=upload_img_user,blank=True)


	def save(self, *args, **kwargs):
		
		try:
			this = CustomUser.objects.get(id=self.id)
			if this.img_profile != self.img_profile:
				this.img_profile.delete()
		except:
			pass

		is_new = self.id is None
		super(CustomUser, self).save()
		
		if is_new:
			self.dni = self.id
			super(CustomUser, self).save()
