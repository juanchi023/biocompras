# Generated by Django 2.0 on 2017-12-22 20:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_promocioes_1_created'),
    ]

    operations = [
        migrations.RenameField(
            model_name='promocioes_1',
            old_name='titulo1',
            new_name='text1',
        ),
        migrations.RenameField(
            model_name='promocioes_1',
            old_name='titulo2',
            new_name='text2',
        ),
        migrations.RenameField(
            model_name='promocioes_1',
            old_name='titulo3',
            new_name='text3',
        ),
        migrations.AddField(
            model_name='promocioes_1',
            name='titulo',
            field=models.CharField(default='Pomo', max_length=40),
            preserve_default=False,
        ),
    ]
