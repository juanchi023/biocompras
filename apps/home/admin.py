from django.contrib import admin
from .models import *


admin.site.register(VideosDestacados)
admin.site.register(TopMarcas)
admin.site.register(MediaSponsors)
admin.site.register(Suscriptor)
admin.site.register(Promocioes_1)