from django.shortcuts import render
from apps.tiendas.models import *
from django.shortcuts import get_object_or_404
from apps.carrito.views import *
from .models import *
from apps.blog.models import *
from django.views.decorators.http import require_POST
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.db.models import Avg, Max, Min, Sum
from django.db.models import Q


def home(request):
	
	return render(request, 'home/index.html',)

def home_compradores(request):

	productos_propios_1 = []
	productos_destacados_1 = []
	productos_recomendados_1 = []
	productos_plus_1 = []
	shop_cart = ""
	cart_items = ""

	productos_propios = Product.objects.filter(store__plan__code='A001')
	for producto in productos_propios:
		precio_menor = ProductDetail.objects.filter(product=producto).aggregate(Min('price'))
		prod = {'id': producto.id,
		'name': producto.name,
		'price':precio_menor,
		'img_url':producto.main_img.url}
		productos_propios_1.append(prod)

	productos_destacados = Product.objects.filter(store__plan__code='A002').order_by('?')[:5]
	for producto in productos_destacados:
		precio_menor = ProductDetail.objects.filter(product=producto).aggregate(Min('price'))
		prod = {'id': producto.id,
		'name': producto.name,
		'price':precio_menor,
		'img_url':producto.main_img.url}
		productos_destacados_1.append(prod)

	productos_recomendados = Product.objects.filter(store__plan__code='A003')
	for producto in productos_recomendados:
		precio_menor = ProductDetail.objects.filter(product=producto).aggregate(Min('price'))
		prod = {'id': producto.id,
		'name': producto.name,
		'price':precio_menor,
		'img_url':producto.main_img.url}
		productos_recomendados_1.append(prod)

	productos_plus = Product.objects.filter(store__plan__code='A004')
	for producto in productos_plus:
		precio_menor = ProductDetail.objects.filter(product=producto).aggregate(Min('price'))
		prod = {'id': producto.id,
		'name': producto.name,
		'price':precio_menor,
		'img_url':producto.main_img.url}
		productos_plus_1.append(prod)

	top_categorias = Category.objects.all()[:6]
	top_tiendas = Store.objects.filter(plan__code='A002')[:5]
	videos = VideosDestacados.objects.all()
	topmarcas = TopMarcas.objects.all()
	mediasponsors = MediaSponsors.objects.all()
	publicaciones = Post.objects.all().order_by('created')[:5]
	promociones = Promocioes_1.objects.all().order_by('created')
	
	try:
		if request.user.is_authenticated:
			shop_cart = ShopCart.objects.get(owner=request.user)
	except ShopCart.DoesNotExist:
		shop_cart = ShopCart()
		shop_cart.owner = request.user
		shop_cart.save()
	try:
		if request.user.is_authenticated:		
			cart_items = CartItems.objects.filter(shopcart=shop_cart)[:3]
	except CartItems.DoesNotExist:
		pass

	return render(request,'home/index-compradores.html',{
    	'productos_propios_1':productos_propios_1,
		'productos_destacados_1':productos_destacados_1,
    	'productos_recomendados_1':productos_recomendados_1,
    	'productos_plus_1':productos_plus_1,
    	'top_categorias':top_categorias,
    	'top_tiendas':top_tiendas,
    	'videos':videos,
    	'topmarcas':topmarcas,
    	'mediasponsors':mediasponsors,
    	'shop_cart':shop_cart,
    	'cart_items':cart_items,
    	'publicaciones':publicaciones,
    	'promociones':promociones})

def home_vendedores(request):
	topmarcas = TopMarcas.objects.all()
	
	return render(request, 'home/index-vendedores.html',{'topmarcas':topmarcas})

def product_detail(request,id_product):
	
	shop_cart = ""
	cart_items = ""

	try:
		shop_cart = ShopCart.objects.get(owner=request.user)
	except ShopCart.DoesNotExist:
		shop_cart = ShopCart()
		shop_cart.owner = request.user
		shop_cart.save()

	try:		
		cart_items = CartItems.objects.filter(shopcart=shop_cart)[:3]
	except CartItems.DoesNotExist:
		pass

	product = get_object_or_404(Product, pk=id_product)
	products_detail = ProductDetail.objects.filter(product=product)
	price = ProductDetail.objects.filter(product=product).aggregate(Min('price'))
	return render(request, 'home/product_detail.html',{'product':product
		,'products_detail':products_detail,'price':price,'shop_cart':shop_cart,
		'cart_items':cart_items})


@require_POST 
def suscribe_newsletter(request):
	form = Suscriptor()
	if request.method == 'POST':
		email = request.POST.get('email', None)
		name = request.POST.get('name', None)

		form.name = name
		form.email = email
		form.save()
		message = 'Su solicitud se ha registrado! Nos comunicaremos con ud. en breve.'
		ctx = {'message': message}
    # use mimetype instead of content_type if django < 5
	return HttpResponse(json.dumps(ctx), content_type='application/json')

def quienes_somos(request):
	return render(request, 'home/quienes_somos.html',)

def contactos(request):
	return render(request, 'home/contactos.html',)