from django.db import models
import os


class VideosDestacados(models.Model):

    def upload_img_video(instance, filename):
    	filename_base, filename_ext = os.path.splitext(filename)
    	return 'home/videos/{0}{1}'.format(filename_base,filename_ext.lower())

    titulo = models.CharField(max_length=40)
    codigo_youtube = models.CharField(max_length=15)
    img_miniatura = models.ImageField(upload_to=upload_img_video)

    def __str__(self):
    	return self.titulo

    class Meta:
    	verbose_name = 'Video Destacado'
    	verbose_name_plural = 'Videos Destacados'

class TopMarcas(models.Model):

	def upload_img_topmarcas(instance, filename):
		filename_base, filename_ext = os.path.splitext(filename)
		return 'home/TopMarcas/{0}{1}'.format(filename_base,filename_ext.lower())

	logo = models.ImageField(upload_to=upload_img_topmarcas)
	name = models.CharField(max_length=40)


	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'TopMarcas'
		verbose_name_plural = 'TopMarcas'


class MediaSponsors(models.Model):

	def upload_img_topmarcas(instance, filename):
		filename_base, filename_ext = os.path.splitext(filename)
		return 'home/MediaSponsors/{0}{1}'.format(filename_base,filename_ext.lower())

	logo = models.ImageField(upload_to=upload_img_topmarcas)
	name = models.CharField(max_length=40)


	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Media Sponsor'
		verbose_name_plural = 'Media Sponsors'



class Suscriptor(models.Model):

	name = models.CharField(max_length=60)
	email = models.CharField(max_length=60)
	suscribe_date = models.DateTimeField(auto_now_add=True)


	def __str__(self):
		return self.email

	class Meta:
		verbose_name = 'Suscritor'
		verbose_name_plural = 'Suscritors'

class Promocioes_1(models.Model):

    def upload_img_promo(instance, filename):
    	filename_base, filename_ext = os.path.splitext(filename)
    	return 'home/promos/{0}{1}'.format(filename_base,filename_ext.lower())
    titulo = models.CharField(max_length=40)
    text1 = models.CharField(max_length=40)
    text2 = models.CharField(max_length=40)
    text3 = models.CharField(max_length=40)
    bg_img = models.ImageField(upload_to=upload_img_promo)
    msj_btn = models.CharField(max_length=40)
    url_btn = models.CharField(max_length=40)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
    	return self.titulo

    class Meta:
    	verbose_name = 'Promoción'
    	verbose_name_plural = 'Promociones'