from django.urls import path
from . import views as home_views


urlpatterns = [
    path('', home_views.home, name='home'),
    path('compra/', home_views.home_compradores, name='home_compradores'),
    path('vende/', home_views.home_vendedores, name='home_vendedores'),
    path('products/detail/<int:id_product>',home_views.product_detail, name='product_detail'),
    path('suscribe_newsletter/', home_views.suscribe_newsletter, name='suscribe_newsletter'),
    path('quienes-somos/', home_views.quienes_somos, name='quienes_somos'),
    path('contactos/', home_views.contactos, name='contactos'),
    ]