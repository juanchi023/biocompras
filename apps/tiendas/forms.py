from django import forms
from .models import Store, Product, StockDetail, ProductDetail

class UpdateStoreForm(forms.ModelForm):

    class Meta():
        model = Store
        fields  = ('name', 'description',)

class CreateProductForm(forms.ModelForm):
    class Meta():
        model = Product
        exclude = ('store','categories',)

class UploadStoreImagesForm(forms.ModelForm):
    class Meta():
        model = Store
        fields  = ('logo', 'cover_img',)

class UpdateProductStock(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UpdateProductStock, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

    class Meta():
        model = StockDetail
        fields  = ('quantity',)

class CreateProductDetailForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CreateProductDetailForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

    class Meta():
        model = ProductDetail
        fields = ('description', 'price',)
