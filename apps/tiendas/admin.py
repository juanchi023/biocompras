from django.contrib import admin
import dropbox
from .models import *

class ProductAdmin(admin.ModelAdmin):    

    actions=['really_delete_selected']

    def get_actions(self, request):
        actions = super(ProductAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def really_delete_selected(self, request, queryset):
        for obj in queryset:
            obj.delete()            
        
        if queryset.count() == 1:
            message_bit = "1 Producto fue"
        else:
            message_bit = "%s Productos fueron" % queryset.count()
        self.message_user(request, "%s borrados exitosamente." % message_bit)
    really_delete_selected.short_description = "Borrar Productos seleccionados"

class ProductImageAdmin(admin.ModelAdmin):    

    actions=['really_delete_selected']

    def get_actions(self, request):
        actions = super(ProductImageAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def really_delete_selected(self, request, queryset):
        for obj in queryset:
            obj.delete()            
        
        if queryset.count() == 1:
            message_bit = "1 Imagen de Producto fue"
        else:
            message_bit = "%s Imágenes Productos fueron" % queryset.count()
        self.message_user(request, "%s borradas exitosamente." % message_bit)
    really_delete_selected.short_description = "Borrar Imágenes Productos seleccionadas"


admin.site.register(Store)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductDetail)
admin.site.register(ProductImage, ProductImageAdmin)
admin.site.register(Category)
admin.site.register(Plan)
admin.site.register(Stock)
admin.site.register(StockDetail)