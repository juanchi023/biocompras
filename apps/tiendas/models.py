from django.db import models, transaction
import os
from django.contrib.auth import get_user_model
from django.template.defaultfilters import slugify
import dropbox
import uuid

from django.db.models import Min


# Create your models here.
"""
Modelo tienda
Este modelo representa el espacio donde el usuario podra administrar sus productos.
"""

User = get_user_model()

class Plan(models.Model):

    code = models.CharField(max_length=4, blank=True)
    name = models.CharField(max_length=30)
    slug = models.SlugField(editable = True, unique=True, blank=True, max_length=30)
    valor = models.BigIntegerField(null=False)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Plan, self).save(*args,**kwargs)

class Store(models.Model):

    def upload_img_store(instance, filename):
        filename_base, filename_ext = os.path.splitext(filename)
        return 'users/{0}/store/{1}{2}'.format(instance.owner.username,filename_base,filename_ext.lower())

    name = models.CharField(max_length=200, blank=True)
    description = models.TextField(blank=True)
    logo = models.ImageField(upload_to=upload_img_store, blank=True)
    cover_img = models.ImageField(upload_to=upload_img_store, blank=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='store', null=True)
    is_active = models.BooleanField(default=False)
    plan = models.ForeignKey(Plan, on_delete=models.CASCADE, null=True)

    def __str__(self):
        if not self.name:
            return ("Tienda de {0}").format(self.owner.username)
        else:
            return ("TIENDA: {0}").format(self.name)


    def save(self, force_insert=False, force_update=False):
        is_new = self.id is None
        super(Store, self).save(force_insert, force_update)
        if is_new:
            Stock.objects.create(store=self)

        if not self.name:
            self.name = "Tienda de {0}".format(self.owner.username)
            super(Store, self).save()


class Product(models.Model):

    def upload_img_store(instance, filename):
        filename_base, filename_ext = os.path.splitext(filename)
        return 'users/{0}/store/products/{1}/{2}{3}'.format(instance.store.owner.username,instance.sku,"main_img",filename_ext.lower())

    name = models.CharField(max_length=255)
    description = models.TextField(blank=False)
    sku = models.CharField(max_length=30)
    store = models.ForeignKey('Store', on_delete=models.CASCADE, related_name='products')
    categories = models.ManyToManyField('Category', related_name='products')
    is_active = models.BooleanField(default=False)
    main_img = models.ImageField(upload_to=upload_img_store, blank=False)
    main_video_youtube_code = models.CharField(max_length=20, blank=True)

    def __str__(self):
        return "Name: %s, Store: %s" % (self.name, self.store)

    def delete(self, *args, **kwargs):
        try:
            dpb = dropbox.Dropbox("JXLOeHROIAAAAAAAAAABtYy4XDEkfdHE4TGlXbyULaWNPgypf0RC7C8id-k4bssZ")
            dpb.files_delete('/biocompras/users/{0}/store/products/{1}'.format(self.store.owner.username,self.id))
        except:
            pass
        super(Product, self).delete(*args, **kwargs)

    @property
    def min_price(self):
        min_value = ProductDetail.objects.filter(product=self).aggregate(Min('price'))
        return min_value['price__min']



class ProductDetail(models.Model):

    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="details")
    short_description = models.CharField(max_length=100)
    description = models.TextField()
    price = models.BigIntegerField(default=0)

    def save(self, force_insert=False, force_update=False):
        is_new = self.id is None
        super(ProductDetail, self).save(force_insert, force_update)
        if is_new:
            StockDetail.objects.create(product_detail=self, stock=self.product.store.stock)

    def __str__(self):
        return "$ {0} - Producto: {1}, Presentación: {2}".format(self.product,self.description,self.price)

class ProductImage(models.Model):

    def upload_img_store(instance, filename):
        filename_base, filename_ext = os.path.splitext(filename)
        return 'users/{0}/store/products/{1}/{2}{3}'.format(instance.product.store.owner.username,instance.product.id,filename_base,filename_ext.lower())

    photo = models.ImageField(upload_to=upload_img_store)
    name = models.CharField(max_length=100)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='photos')

    def __str__(self):
        return self.product.name

    def delete(self, *args, **kwargs):
        dpb = dropbox.Dropbox("JXLOeHROIAAAAAAAAAABtYy4XDEkfdHE4TGlXbyULaWNPgypf0RC7C8id-k4bssZ")

        dpb.files_delete('/biocompras/{0}'.format(self.photo))
        print("borre")
        super(ProductImage, self).delete(*args, **kwargs)

class Category(models.Model):
    name = models.CharField(max_length=128)
    slug = models.SlugField(editable = True, unique=True, blank=True, max_length=30)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args,**kwargs)

class Subcategory(models.Model):
    category = models.ForeignKey(Category,on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    slug = models.SlugField(editable = True, unique=True, blank=True, max_length=30)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args,**kwargs)

class Stock(models.Model):
    store = models.OneToOneField(Store, related_name='stock', on_delete=models.CASCADE)
    last_update = models.DateTimeField(auto_now_add=True)

class StockDetail(models.Model):
    stock = models.ForeignKey(Stock, related_name='stocks', on_delete=models.CASCADE)
    product_detail = models.OneToOneField(ProductDetail, related_name="stocks", on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=0)

class ShopOrder(models.Model):

    ref = models.CharField(max_length=100, unique=True)
    client = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.TextField(null=True, blank=True)

    PENDIENTE = 0
    PROCESADA = 1
    CANCELADA = 2

    STATUS_CHOICES = (
        (PENDIENTE, 'Pendiente'),
        (PROCESADA, 'Procesada'),
        (CANCELADA, 'Cancelada'))

    status = models.IntegerField(choices=STATUS_CHOICES,default=PENDIENTE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    total_payed = models.BigIntegerField(default=0)

    def save(self, *args, **kwargs):
        if not self.id:
            self.ref = uuid.uuid4().hex[:5]
        super(ShopOrder, self).save(*args, **kwargs)
