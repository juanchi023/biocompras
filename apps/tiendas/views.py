from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .forms import *
from .models import *
from django.urls import reverse


def admin_store(request):
    store = request.user.store.get()
    form = UpdateStoreForm(request.POST or None, instance=store)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('panel_store'))
    return render(request, 'stores/stores/panel/create_store.html', {'form':form,'errors':form.errors})

def panel_store(request):
    if not request.user.is_vendor:
        return HttpResponseRedirect(reverse('plans'))

    products = request.user.store.get().products.all()[:5]
    context = { 'products':products }
    return render(request, 'stores/stores/panel/admin_store.html', context)

def upload_img_store(request):
    store = request.user.store.get()
    form = UploadStoreImagesForm(request.POST or None,request.FILES or None, instance=store)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('panel_store'))
    return render(request, 'stores/stores/panel/update_images_store.html', {'form':form })

def create_product(request):

    form_product = CreateProductForm(request.POST or None, request.FILES or None)
    form_detail = CreateProductDetailForm(request.POST or None)
    form_stock = UpdateProductStock(request.POST or None)

    context = {
        'form_product' : form_product,
    }

    if form_product.is_valid():
        product = form_product.save(commit=False)
        product.store = request.user.store.first()
        product.save()

        descriptions = request.POST.getlist("description_pr")
        prices = request.POST.getlist("price")
        quantities = request.POST.getlist("quantity")

        print("Descripciones: %d " % len(descriptions))
        print("Precios: %d " % len(prices))
        print("Cantidades: %d " % len(quantities))


        for desc, prc, qty in zip(descriptions, prices, quantities):
            detail = ProductDetail()
            detail.description = desc
            detail.price = prc
            detail.product = product
            detail.save()

            stock = detail.stocks
            stock.quantity = qty
            stock.save()

        return HttpResponseRedirect(reverse('panel_store'))

    return render(request, 'stores/stores/products/create_product_detail.html', context)



def product_stock(request, id_product):
    product_detail = get_object_or_404(ProductDetail,pk=id_product)
    stock = product_detail.stocks
    form = UpdateProductStock(request.POST or None, instance=stock)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('my_stock'))
    return render(request, 'stores/stores/products/stock_detail.html', {'form':form, 'product':product, 'stock':stock })

def list_my_products(request):
    store = request.user.store.first()
    products = Product.objects.filter(store=store)
    return render(request, 'stores/stores/products/my_products.html',{'products':products})

def store_stock(request):
    stock = request.user.store.first().stock
    stocks = stock.stocks.all()
    return render(request, 'stores/stores/panel/stock.html',{'stocks':stocks})

def order_list(request):
    return render(request, 'stores/stores/panel/order_list.html')

def plans(request):
    return render(request, 'stores/stores/plans.html')

def activate_plan(request, id_plan):
    plan = get_object_or_404(Plan, pk=id_plan)
    store = request.user.store.first()
    store.plan = plan;
    store.save()
    request.user.is_vendor = True
    request.user.save()
    return HttpResponseRedirect(reverse('panel_store'))

def product_detail(request, id_product):
    product = get_object_or_404(Product, pk=id_product)
    return render(request, 'stores/stores/products/product_detail_admin.html',{'product':product })

def update_product_detail(request, id_product_detail):
    detail = get_object_or_404(ProductDetail, pk=id_product_detail)
    form_detail = CreateProductDetailForm(request.POST or None, instance = detail)
    form_stock = UpdateProductStock(request.POST or None, instance = detail.stocks)
    context = {
        'form_detail': form_detail,
        'form_stock' : form_stock,
        'detail' : detail
    }

    if form_detail.is_valid() and form_stock.is_valid:
        form_detail.save()
        form_stock.save()
        return HttpResponseRedirect(reverse('product_detail_admin', kwargs= { 'id_product' : detail.product.id}))
    return render(request, 'stores/stores/products/update_product_detail.html', context)
