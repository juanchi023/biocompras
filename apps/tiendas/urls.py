from django.urls import path

from . import views

urlpatterns = [
    path('',views.plans, name='plans'),
    path('plans/<int:id_plan>', views.activate_plan, name='activate_plan'),
    path('create_store/', views.admin_store, name='create_store'),
    path('my_store/', views.panel_store, name='panel_store'),
    path('my_store/create_product', views.create_product, name='create_product'),
    #path('my_store/public_product/<int:id_product>/', views.activate_product, name='activate_product'),
    path('my_store/my_products/',views.list_my_products, name='my_products'),
    path('my_store/my_stock/',views.store_stock, name='my_stock'),
    path('my_store/orders/',views.order_list, name='order_list'),
    path('my_store/update_images_store/',views.upload_img_store, name='update_images_store'),
    path('my_store/products/<int:id_product>',views.product_detail, name='product_detail_admin'),
    path('my_store/products/<int:id_product>/stock',views.product_stock, name='product_stock'),
    path('my_store/products/detail/<int:id_product_detail>',views.update_product_detail, name='update_product_detail'),


]
