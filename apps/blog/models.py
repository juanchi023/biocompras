import os
from django.contrib.auth import get_user_model 
from django.db import models
from django.template.defaultfilters import slugify
from django.utils.encoding import python_2_unicode_compatible

User = get_user_model() 

class TimeStampModel(models.Model):

	user = models.ForeignKey(User, db_index=True, null=True, blank=True, on_delete=models.CASCADE)
	description = models.TextField()
	created = models.DateTimeField(auto_now_add = True)
	modified = models.DateTimeField(auto_now = True)

	class Meta:
		abstract = True 

class Categoria(models.Model):

	nombre = models.CharField(max_length=40)
	slug = models.SlugField(editable = False, unique=True, null=True, blank=True, max_length=255)

	def __str__(self):
		return "{0}".format(self.nombre)

	def save(self, *args, **kwargs):
		if not self.id:
			self.slug = slugify(self.nombre)
		super(Categoria, self).save(*args,**kwargs)
		
def upload_imagen_post(instance, filename):
        filename_base, filename_ext = os.path.splitext(filename)
        return 'blog/post/{0}/img/{1}{2}'.format(instance.slug,filename_base,filename_ext.lower())


class Post(TimeStampModel): 

	imagen = models.ImageField(upload_to=upload_imagen_post, blank=True)
	titulo = models.CharField(max_length=400, editable = True)
	categoria = models.ManyToManyField(Categoria)
	slug = models.SlugField(editable = True, unique=True, null=True, blank=True,max_length=400)
	vistas = models.BigIntegerField(default=0)
	descripcion_corta = models.CharField(max_length=200)
	
	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = slugify(self.titulo)
		super(Post, self).save(*args,**kwargs)

	def __str__(self):
		return "{0}".format(self.titulo)

class Comment(models.Model):

	user = models.CharField(max_length=500, blank=False)
	email = models.CharField(max_length=500, blank=False)
	description = models.TextField()
	created = models.DateTimeField(auto_now_add = True)
	post = models.ForeignKey(Post, on_delete=models.CASCADE)

	def __str__(self):
		return "{0} - {1} - {2}".format(self.post.titulo, self.user, self.email)


