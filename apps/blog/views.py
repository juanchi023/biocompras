from django.shortcuts import render, redirect
from .models import *
from django.views.generic import ListView, CreateView, DetailView
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.db.models import Q
 

def home_blog(request):
 
    publicaciones = Post.objects.all() 
    paginator = Paginator(publicaciones, 5) # Show 25 contacts per page

    page = request.GET.get('page')
    posts = paginator.get_page(page)

    categories = Categoria.objects.all()

    return render(request,'blog/index.html',{'posts':posts,'categories':categories})

def busqueda_palabra_clave(request):

	categories = Categoria.objects.all()

	busqueda = request.GET.get('busqueda')
		
	publicaciones = Post.objects.filter(
		Q(titulo__icontains=busqueda)
		| Q(categoria__nombre__icontains=busqueda)			
		).distinct()

	paginator = Paginator(publicaciones, 5)
	try:
		page = request.GET['page']
	except:
		page = 1
	posts = paginator.get_page(page)	

	return render(request,'blog/busqueda-palabra-clave.html',{'posts':posts
		,'busqueda':busqueda,'categories':categories})

def filtro_categoria(request, *args, **kwargs):

	categories = Categoria.objects.all()

	category = Categoria.objects.get(slug=kwargs.get('slug'))
		
	publicaciones = Post.objects.filter(categoria__id=category.id)

	paginator = Paginator(publicaciones, 5)
	try:
		page = request.GET['page']
	except:
		page = 1
	posts = paginator.get_page(page)

	return render(request,'blog/busqueda-categoria.html',{'posts':posts
		,'category':category,'categories':categories})

def single_post(request, *args, **kwargs):
 	list_posts = []
 	post = Post.objects.get(slug=kwargs.get('slug'))
 	categories_post = post.categoria.all()
 	for categoria in categories_post:
 		related_post = Post.objects.filter(categoria=categoria)
 		for pub in related_post:
 			if pub != post:
 				list_posts.append(pub)
 	categories = Categoria.objects.all()
 	comments = Comment.objects.filter(post__slug=kwargs.get('slug'))
 	print(list_posts)
 	return render(request,'blog/single-post.html',{'post':post
    	,'categories':categories
    	,'list_posts':list_posts
    	,'comments':comments})

@require_POST
def post_comment(request, *args, **kwargs):
	
	if request.method == 'POST':
		email = request.POST.get('email', None)
		user = request.POST.get('user', None)
		mensaje = request.POST.get('comment', None)
		slug = request.POST.get('slug', None)

		comment = Comment()
		comment.user = user
		comment.email = email
		comment.description = mensaje
		comment.post = Post.objects.get(slug = slug)
		comment.save()

		message = 'Su comentario se ha enviado!'

		ctx = {'message': message}
		
		return HttpResponse(json.dumps(ctx), content_type='application/json')