from django import template

register = template.Library()

@register.filter
def get_count_posts(post):

	num = post.count()

	return num