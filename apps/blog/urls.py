from django.urls import path
from . import views


urlpatterns = [
    path('blog/', views.home_blog, name='home_blog'),
    path('blog/busqueda/', views.busqueda_palabra_clave, name='busqueda_palabra_clave'),
    path('blog/categoria/<slug:slug>',views.filtro_categoria, name='filtro_categoria'),
    path('blog/post/<slug:slug>',views.single_post, name='single_post'),
    path('blog/post/post-comment/',views.post_comment, name='post_comment'),
    ]
    